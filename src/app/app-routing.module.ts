import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddressConfirmComponent } from './pages/address-confirm/address-confirm.component';
import { AddressComponent } from './pages/address/address.component';
import { HomePageComponent } from './pages/home/home.page.component';
import { OrderSummaryComponent } from './pages/order-summary/order-summary.component';
import { OrdersComponent } from './pages/orders/orders.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { SelectCategoryComponent } from './pages/select-category/select-category.component';
import { SelectMenuComponent } from './pages/select-menu/select-menu.component';
import { SelectProductComponent } from './pages/select-product/select-product.component';
import { TimeComponent } from './pages/time/time.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'select-category', component: SelectCategoryComponent },
  { path: 'select-product/:category', component: SelectProductComponent },
  { path: 'select-menu', component: SelectMenuComponent },
  { path: 'order', component: OrderSummaryComponent },
  { path: 'order/address', component: AddressComponent },
  { path: 'order/address/confirm', component: AddressConfirmComponent },
  { path: 'order/time', component: TimeComponent },
  { path: 'order/payment', component: PaymentComponent },
  { path: 'orders', component: OrdersComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
