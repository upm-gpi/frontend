import { Component, HostBinding, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-subtitle',
  templateUrl: './subtitle.component.html',
  styleUrls: ['./subtitle.component.scss'],
})
export class SubtitleComponent implements OnInit {
  @Input()
  fontSize: string = '4rem';

  @Input() color: string = '#bcb2c0';

  @Input() lineHeight: string = 'initial';

  constructor() {}

  ngOnInit(): void {}
}
