import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-nav-bar-element',
  template: `
    <a
      mat-flat-button
      [routerLink]="link"
      routerLinkActive="selected"
      [routerLinkActiveOptions]="{ exact: true }"
      ><ng-content></ng-content
    ></a>
  `,
  styles: [
    `
      a {
        text-decoration: none;
        color: black;
        font-weight: 400;
        font-size: 2rem;
      }

      .selected {
        color: white;
        background: linear-gradient(
          40deg,
          rgba(0, 51, 199, 0.7),
          rgba(209, 149, 249, 0.7)
        );
        padding: 0.2rem 0.5rem;
        border-radius: 1rem;
      }
    `,
  ],
})
export class NavBarElementComponent {
  @Input() link: string;
}
