import { HostBinding } from '@angular/core';
import { Component, Input } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
})
export class NavBarComponent {
  @HostBinding('style.position')
  @Input()
  position: 'fixed' | 'sticky' = 'fixed';

  constructor(public authService: AuthService) {}
}
