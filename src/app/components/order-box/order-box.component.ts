import { Component, Input, OnInit } from '@angular/core';
import { Order } from 'src/app/model/Order';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-order-box',
  templateUrl: './order-box.component.html',
  styleUrls: ['./order-box.component.scss'],
})
export class OrderBoxComponent implements OnInit {
  @Input() order: Order;

  constructor(public modalService: ModalService) {}

  ngOnInit(): void {}
}
