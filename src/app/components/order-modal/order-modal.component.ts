import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Order, OrderState } from 'src/app/model/Order';
import { AuthService } from 'src/app/services/auth.service';
import { ModalService } from 'src/app/services/modal.service';
import { OrdersService } from 'src/app/services/orders.service';

@Component({
  selector: 'app-order-modal',
  templateUrl: './order-modal.component.html',
  styleUrls: ['./order-modal.component.scss'],
})
export class OrderModalComponent implements OnInit {
  subscriptions: Subscription[] = [];
  order: Order;

  constructor(
    @Inject(MAT_DIALOG_DATA) data: { order: Order },
    public ordersService: OrdersService,
    public authService: AuthService,
    private modalService: ModalService
  ) {
    this.order = data.order;
  }

  ngOnInit(): void {}

  getOrderStateOptions(): OrderState[] {
    return Object.values(OrderState);
  }

  changeOrderState(newState: OrderState) {
    this.subscriptions.push(
      this.ordersService.changeOrderState(this.order.id, newState).subscribe({
        next: () => (this.order.state = newState),
        error: () =>
          this.modalService.openGenericModal({
            title: 'Cambio de estado de pedido',
            message: 'No se ha podido cambiar el estado del pedido',
          }),
      })
    );
  }

  deleteOrder() {
    this.subscriptions.push(
      this.ordersService.deleteOrder(this.order).subscribe({
        next: () => (location as any).reload(),
        error: () =>
          this.modalService.openGenericModal({
            title: 'Cancelar pedido',
            message: 'No se ha podido cancelar el pedido',
          }),
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subs) => subs.unsubscribe());
    this.subscriptions = [];
  }
}
