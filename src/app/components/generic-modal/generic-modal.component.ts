import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

export type GenericModalData = {
  title: string;
  message: string;
};

@Component({
  selector: 'app-generic-modal',
  templateUrl: './generic-modal.component.html',
  styleUrls: ['./generic-modal.component.scss'],
})
export class GenericModalComponent implements OnInit {
  data: GenericModalData;
  constructor(@Inject(MAT_DIALOG_DATA) data: { display: GenericModalData }) {
    this.data = data.display;
  }

  ngOnInit(): void {}
}
