import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss'],
})
export class ShoppingCartComponent implements OnInit {
  constructor(
    public shoppingCartService: ShoppingCartService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  goToOrderSummary() {
    this.router.navigate(['/order']);
  }
}
