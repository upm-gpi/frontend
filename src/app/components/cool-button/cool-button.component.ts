import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-cool-button',
  templateUrl: './cool-button.component.html',
  styleUrls: ['./cool-button.component.scss'],
})
export class CoolButtonComponent implements OnInit {
  @Input() disabled: boolean = false;
  @Output() onClick = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  handleClick() {
    if (!this.disabled) {
      this.onClick.emit();
    }
  }
}
