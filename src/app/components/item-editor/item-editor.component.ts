import { Component, Input, OnInit } from '@angular/core';
import {
  ItemEntry,
  ShoppingCartService,
} from 'src/app/services/shopping-cart.service';

@Component({
  selector: 'app-item-editor',
  templateUrl: './item-editor.component.html',
  styleUrls: ['./item-editor.component.scss'],
})
export class ItemEditorComponent implements OnInit {
  @Input() itemEntry: ItemEntry<any>;

  constructor(private shoppingCardService: ShoppingCartService) {}

  ngOnInit(): void {}

  add() {
    this.shoppingCardService.addItem(this.itemEntry.item);
  }

  remove() {
    this.shoppingCardService.removeItem(this.itemEntry.item);
  }
}
