import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { InitService } from 'src/app/services/init.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-auth-button',
  templateUrl: './auth-button.component.html',
  styleUrls: ['./auth-button.component.scss'],
})
export class AuthButtonComponent implements OnInit {
  constructor(
    public modalService: ModalService,
    public authService: AuthService,
    private initService: InitService
  ) {}

  ngOnInit(): void {}

  handleClick() {
    if (this.authService.isLoggedIn()) {
      this.initService.clearData();
    } else {
      this.modalService.openAuthModal();
    }
  }
}
