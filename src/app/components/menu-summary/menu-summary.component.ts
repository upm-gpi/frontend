import { Component, OnInit } from '@angular/core';
import { Menu } from 'src/app/model/Menu';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';

@Component({
  selector: 'app-menu-summary',
  templateUrl: './menu-summary.component.html',
  styleUrls: ['./menu-summary.component.scss'],
})
export class MenuSummaryComponent implements OnInit {
  constructor(public shoppingCartService: ShoppingCartService) {}

  ngOnInit(): void {}
}
