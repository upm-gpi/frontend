import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { LoginRequest } from 'src/app/model/request/LoginRequest';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginFormData: LoginRequest = new LoginRequest();
  @Input() disabled: boolean = false;
  @Output() onSubmit = new EventEmitter<LoginRequest>();

  constructor() {}

  ngOnInit(): void {}

  handleSubmit() {
    this.onSubmit.emit(this.loginFormData);
  }
}
