import { HttpErrorResponse } from '@angular/common/http';
import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Observer, PartialObserver } from 'rxjs';
import { LoginRequest } from 'src/app/model/request/LoginRequest';
import { RegisterRequest } from 'src/app/model/request/RegisterRequest';
import { User } from 'src/app/model/User';
import { AuthService } from 'src/app/services/auth.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-auth-modal',
  templateUrl: './auth-modal.component.html',
  styleUrls: ['./auth-modal.component.scss'],
})
export class AuthModalComponent implements OnInit {
  mode: 'login' | 'register' = 'login';
  showSpinner: boolean = false;
  errorMsg: string;

  handleError = (httpError: HttpErrorResponse) => {
    switch (httpError.status) {
      case 401:
      case 404:
        this.errorMsg = 'usuario y/o contraseña incorrectos';
        break;

      case 400:
        const { field, defaultMessage } = httpError.error['errors'][0];
        this.errorMsg = `${field}: ${defaultMessage}`;
        break;

      default:
        this.errorMsg = 'Ha ocurrido un error en el proceso';
    }

    this.showSpinner = false;
  };

  readonly OBSERVER: PartialObserver<User> = {
    next: () => this.modalService.closeAuthModal(),
    error: this.handleError,
  };

  constructor(
    private authService: AuthService,
    private modalService: ModalService
  ) {}

  ngOnInit(): void {}

  login(loginRequestData: LoginRequest) {
    this.showSpinner = true;
    this.authService.login(loginRequestData).subscribe(this.OBSERVER);
  }

  register(registerRequestData: RegisterRequest) {
    this.showSpinner = true;
    this.authService.register(registerRequestData).subscribe(this.OBSERVER);
  }

  changeMode() {
    this.mode = this.mode === 'login' ? 'register' : 'login';
    this.errorMsg = '';
  }

  getLiterals() {
    let title, buttonLabel;

    if (this.mode === 'login') {
      title = 'LOGIN';
      buttonLabel = 'Registrarme';
    } else {
      // 'register'
      title = 'REGISTRO';
      buttonLabel = 'Iniciar sesión';
    }

    return { title, buttonLabel };
  }
}
