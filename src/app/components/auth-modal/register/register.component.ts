import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RegisterRequest } from 'src/app/model/request/RegisterRequest';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  registerFormData: RegisterRequest = new RegisterRequest();
  @Input() disabled: boolean = false;
  @Output() onSubmit = new EventEmitter<RegisterRequest>();

  constructor() {}

  ngOnInit(): void {}

  handleSubmit() {
    this.onSubmit.emit(this.registerFormData);
  }
}
