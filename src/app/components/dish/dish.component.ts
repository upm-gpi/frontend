import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/model/Product';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';

@Component({
  selector: 'app-dish',
  templateUrl: './dish.component.html',
  styleUrls: ['./dish.component.scss'],
})
export class DishComponent implements OnInit {
  @Input() product: Product;

  constructor(public shoppingCartService: ShoppingCartService) {}

  ngOnInit(): void {}

  addProduct() {
    this.product && this.shoppingCartService.addItem(this.product);
  }

  removeProduct() {
    this.product && this.shoppingCartService.removeItem(this.product);
  }

  readonly IMAGE_PATHS = {
    entrante: 'assets/categories/starter.jpg',
    ensalada: 'assets/categories/salad.jpg',
    principal: 'assets/categories/main.jpg',
    postre: 'assets/categories/dessert.jpg',
    bebida: 'assets/categories/drink.jpg',
  };

  isSelected(): boolean {
    return this.shoppingCartService.isInCart(this.product);
  }
}
