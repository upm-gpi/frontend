import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { HttpClientModule } from '@angular/common/http';
import { HomePageComponent } from './pages/home/home.page.component';
import { CenterLayoutComponent } from './layouts/center/center.layout.component';
import { NavBarElementComponent } from './components/nav-bar/nav-bar-element/nav-bar-element.component';
import { SelectCategoryComponent } from './pages/select-category/select-category.component';
import { SelectProductComponent } from './pages/select-product/select-product.component';
import { SelectMenuComponent } from './pages/select-menu/select-menu.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { AuthModalComponent } from './components/auth-modal/auth-modal.component';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './components/auth-modal/login/login.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AuthButtonComponent } from './components/auth-button/auth-button.component';
import { RegisterComponent } from './components/auth-modal/register/register.component';
import { DishComponent } from './components/dish/dish.component';
import { CoolButtonComponent } from './components/cool-button/cool-button.component';
import { TitleComponent } from './components/title/title.component';
import { SubtitleComponent } from './components/subtitle/subtitle.component';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import { MatRadioModule } from '@angular/material/radio';
import { OrderSummaryComponent } from './pages/order-summary/order-summary.component';
import { AddressComponent } from './pages/address/address.component';
import { AddressConfirmComponent } from './pages/address-confirm/address-confirm.component';
import { TimeComponent } from './pages/time/time.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { OrdersComponent } from './pages/orders/orders.component';
import { OrderBoxComponent } from './components/order-box/order-box.component';
import { OrderModalComponent } from './components/order-modal/order-modal.component';
import { GenericModalComponent } from './components/generic-modal/generic-modal.component';
import { ProductSummaryComponent } from './components/product-summary/product-summary.component';
import { MenuSummaryComponent } from './components/menu-summary/menu-summary.component';
import { MatBadgeModule } from '@angular/material/badge';
import { ItemEditorComponent } from './components/item-editor/item-editor.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    HomePageComponent,
    CenterLayoutComponent,
    NavBarElementComponent,
    SelectCategoryComponent,
    SelectProductComponent,
    SelectMenuComponent,
    AuthModalComponent,
    LoginComponent,
    AuthButtonComponent,
    RegisterComponent,
    DishComponent,
    CoolButtonComponent,
    TitleComponent,
    SubtitleComponent,
    ShoppingCartComponent,
    OrderSummaryComponent,
    AddressComponent,
    AddressConfirmComponent,
    TimeComponent,
    PaymentComponent,
    OrdersComponent,
    OrderBoxComponent,
    OrderModalComponent,
    GenericModalComponent,
    ProductSummaryComponent,
    MenuSummaryComponent,
    ItemEditorComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatCardModule,
    MatButtonModule,
    FormsModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatBadgeModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
