import { Injectable } from '@angular/core';
import {
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from '@angular/material/dialog';
import { AuthModalComponent } from '../components/auth-modal/auth-modal.component';
import {
  GenericModalComponent,
  GenericModalData,
} from '../components/generic-modal/generic-modal.component';
import { OrderModalComponent } from '../components/order-modal/order-modal.component';
import { Order } from '../model/Order';

@Injectable({
  providedIn: 'root',
})
export class ModalService {
  readonly MODAL_CONFIG: MatDialogConfig = {
    autoFocus: false,
    position: { top: '20vh' },
  };
  constructor(private materialDialogService: MatDialog) {}

  authModalRef: MatDialogRef<AuthModalComponent>;
  openAuthModal() {
    this.authModalRef = this.materialDialogService.open(
      AuthModalComponent,
      this.MODAL_CONFIG
    );
  }
  closeAuthModal() {
    this.authModalRef && this.authModalRef.close();
  }

  orderModal: MatDialogRef<OrderModalComponent>;
  openOrderModal(order: Order) {
    this.orderModal = this.materialDialogService.open(OrderModalComponent, {
      ...this.MODAL_CONFIG,
      data: {
        order,
      },
    });
  }
  closeOrderModal() {
    this.orderModal && this.orderModal.close();
  }

  genericModal: MatDialogRef<GenericModalComponent>;
  openGenericModal(data: GenericModalData) {
    this.genericModal = this.materialDialogService.open(GenericModalComponent, {
      ...this.MODAL_CONFIG,
      data: {
        display: data,
      },
    });
  }
  closeGenericModal() {
    this.genericModal && this.genericModal.close();
  }
}
