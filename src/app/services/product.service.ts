import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../model/Product';
import { DailyMenuResponse } from '../model/response/DailyMenuResponse';
import { ApiService } from './api.service';

export type Filters = {
  category?: string;
  inMenu?: boolean;
  inDailyMenu?: boolean;
};

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private api: ApiService) {}

  getProductsBy(filters: Filters): Observable<Product[]> {
    return this.api.get<Product[]>('products', filters);
  }

  getProducts(): Observable<Product[]> {
    return this.getProductsBy({});
  }

  getCategories(): Observable<string[]> {
    return this.api.get('products/categories');
  }

  getDailyMenu(): Observable<DailyMenuResponse> {
    return this.api.get('products/dailyMenu');
  }
}
