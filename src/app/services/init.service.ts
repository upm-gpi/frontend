import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { OrdersService } from './orders.service';
import { ShoppingCartService } from './shopping-cart.service';

@Injectable({
  providedIn: 'root',
})
export class InitService {
  constructor(
    private authService: AuthService,
    private shoppingCartService: ShoppingCartService,
    private ordersService: OrdersService
  ) {}

  loadData() {
    this.authService.loadDataFromStorage();
    this.shoppingCartService.loadFromStorage();
  }

  clearData() {
    this.authService.logout();
    this.shoppingCartService.clear();
    this.ordersService.clearData();
  }
}
