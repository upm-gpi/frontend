import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoginRequest } from '../model/request/LoginRequest';
import { RegisterRequest } from '../model/request/RegisterRequest';
import { User } from '../model/User';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  user: User;
  constructor(private api: ApiService) {}

  login(data: LoginRequest): Observable<User> {
    return this.api.post('login', data).pipe(tap(this.saveUser));
  }

  register(data: RegisterRequest): Observable<User> {
    return this.api
      .post('register', { ...data, role: 'cliente' })
      .pipe(tap(this.saveUser));
  }

  loadDataFromStorage() {
    const data = localStorage.getItem('user');
    if (data) {
      this.user = JSON.parse(data);
    }
  }

  isLoggedIn(): boolean {
    return !!this.user?.id;
  }

  logout() {
    this.user = null;
    localStorage.removeItem('user');
  }

  private saveUser = (user: User) => {
    this.user = user;
    localStorage.setItem('user', JSON.stringify(user));
  };
}
