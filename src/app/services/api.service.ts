import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  readonly SERVER = 'https://gpi-grupo1.westeurope.cloudapp.azure.com';
  // readonly SERVER = 'http://localhost:8080';

  constructor(private http: HttpClient) {}

  get<T>(endpoint: string, params?: any): Observable<T> {
    return this.request('get', endpoint, { params });
  }

  post<T>(endpoint: string, body?: any): Observable<T> {
    return this.request('post', endpoint, { body });
  }

  patch<T>(endpoint: string, body?: any): Observable<T> {
    return this.request('patch', endpoint, { body });
  }

  delete<T>(endpoint: string, body?: any): Observable<T> {
    return this.request('delete', endpoint, { body });
  }

  request<T>(
    method: 'get' | 'post' | 'patch' | 'delete',
    endpoint: string,
    params?: { body?: any; params?: any }
  ): Observable<T> {
    return this.http.request<T>(method, `${this.SERVER}/${endpoint}`, params);
  }
}
