import { Injectable } from '@angular/core';
import { Menu } from '../model/Menu';
import { Product } from '../model/Product';

type CartItem = Product | Menu;
export type ItemEntry<T> = {
  item: T;
  count: number;
};
type CartSection<T> = Map<number, ItemEntry<T>>; // id -> entry

@Injectable({
  providedIn: 'root',
})
export class ShoppingCartService {
  private products: CartSection<Product> = new Map();
  private menus: CartSection<Menu> = new Map();

  constructor() {}

  getSection<T extends CartItem>(item: T): CartSection<T> {
    const isProduct = !!(item as Product)?.category;
    const isMenu = !!(item as Menu)?.content;
    return (isProduct ? this.products : isMenu ? this.menus : null) as any;
  }

  numberOfSelections(item: CartItem): number {
    return this.getSection(item).get(item.id)?.count || 0;
  }

  addItem(item: CartItem) {
    this.getSection(item).set(item.id, {
      item,
      count: this.numberOfSelections(item) + 1,
    });

    this.updateStorage();
  }

  removeItem(item: CartItem) {
    const numberOfOccurences = this.numberOfSelections(item);
    const cartSection = this.getSection(item);

    if (numberOfOccurences > 1) {
      cartSection.set(item.id, {
        item,
        count: numberOfOccurences - 1,
      });
    } else {
      cartSection.delete(item.id);
    }

    this.updateStorage();
  }

  isInCart(item: CartItem): boolean {
    const itemSection = this.getSection(item);
    return item && itemSection.size > 0 && itemSection.has(item.id);
  }

  isEmpty(): boolean {
    return this.products.size === 0 && this.menus.size === 0;
  }

  private sumArray = (arr: number[]) =>
    arr.reduce((acc, curr) => acc + curr, 0);

  totalNumber(): number {
    return this.sumArray(
      Array.from([...this.products.values(), ...this.menus.values()]).map(
        (itemEntry) => itemEntry.count
      )
    );
  }

  readonly VALUE_OF_MENU = 12; // €
  private amountOfMenus(): number {
    return (
      this.sumArray(
        Array.from(this.menus.values()).map((menuEntry) => menuEntry.count)
      ) * this.VALUE_OF_MENU
    );
  }

  private allProductsPrice(): number {
    return this.sumArray(
      Array.from(this.products.values()).map(
        (productEntry) => productEntry.count * productEntry.item.price
      )
    );
  }

  totalAmount(): number {
    return this.amountOfMenus() + this.allProductsPrice();
  }

  getProducts(): ItemEntry<Product>[] {
    return Array.from(this.products.values());
  }

  getMenus(): ItemEntry<Menu>[] {
    return Array.from(this.menus.values());
  }

  clear() {
    this.products = new Map();
    this.menus = new Map();
    this.updateStorage();
  }

  private updateStorage() {
    localStorage.setItem(
      'products',
      JSON.stringify(Array.from(this.products.entries()))
    );
    localStorage.setItem(
      'menus',
      JSON.stringify(Array.from(this.menus.entries()))
    );
  }

  loadFromStorage() {
    this.products = new Map(JSON.parse(localStorage.getItem('products')));
    this.menus = new Map(JSON.parse(localStorage.getItem('menus')));
  }
}
