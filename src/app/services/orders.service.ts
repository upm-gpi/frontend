import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Order, OrderState } from '../model/Order';
import { OrderRequest } from '../model/request/OrderRequest';
import { ApiService } from './api.service';
import { AuthService } from './auth.service';
import { ShoppingCartService } from './shopping-cart.service';

@Injectable({
  providedIn: 'root',
})
export class OrdersService {
  constructor(
    private api: ApiService,
    private authService: AuthService,
    private shoppingCartService: ShoppingCartService
  ) {}

  getOrders(): Observable<Order[]> {
    if (!this.authService.isLoggedIn()) {
      return of(this.getFromStorage());
    }

    const userId = this.authService.user.id;
    return this.api.get('orders', { customerId: userId });
  }

  changeOrderState(orderId: number, newState: OrderState): Observable<void> {
    if (!this.authService.isLoggedIn()) {
      return of();
    }

    const userId = this.authService.user.id;
    return this.api.patch(`order/state`, { userId, orderId, newState });
  }

  createNewOrder(
    deliveryTime: number,
    shippingAddress: string
  ): Observable<Order> {
    const request: OrderRequest = {
      deliveryTime,
      shippingAddress,
      amount: this.shoppingCartService.totalAmount(),
      userId: this.authService?.user?.id,
      products: this.shoppingCartService.getProducts().map((productEntry) => ({
        amount: productEntry.count,
        id: productEntry.item.id,
      })),
      menus: this.shoppingCartService.getMenus().map((menuEntry) => ({
        amount: menuEntry.count,
        starterId: menuEntry.item.content.starter.id,
        mainId: menuEntry.item.content.main.id,
        dessertId: menuEntry.item.content.dessert.id,
        drinkId: menuEntry.item.content.drink.id,
      })),
    };

    return this.api.post<Order>('order', request).pipe(
      tap((order) => {
        this.shoppingCartService.clear();
        this.saveToStorage(order);
      })
    );
  }

  private getFromStorage(): Order[] {
    return JSON.parse(localStorage.getItem('orders')) || [];
  }

  private saveToStorage(order: Order) {
    const currentList = this.getFromStorage();
    currentList.push(order);
    localStorage.setItem('orders', JSON.stringify(currentList));
  }

  private removeFromStorage(order: Order) {
    const currentList = this.getFromStorage();
    currentList.splice(
      currentList.findIndex((o) => o.id === order.id),
      1
    );
    localStorage.setItem('orders', JSON.stringify(currentList));
  }

  clearData() {
    localStorage.removeItem('orders');
  }

  canDeleteOrder(order: Order): boolean {
    if (!this.authService.isLoggedIn()) {
      return false;
    }

    const { id, role } = this.authService.user;
    if (role === 'administrador') {
      return true;
    }

    return role === 'cliente' && id === order.user.id;
  }

  deleteOrder(order: Order): Observable<any> {
    return this.api
      .delete('order', {
        userId: this.authService.user?.id,
        orderId: order.id,
      })
      .pipe(tap(() => this.removeFromStorage(order)));
  }
}
