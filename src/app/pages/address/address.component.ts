import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddressService } from 'src/app/services/address.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss'],
})
export class AddressComponent implements OnInit {
  constructor(
    private router: Router,
    public location: Location,
    public addressService: AddressService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {}

  navigateToAddressConfirmation() {
    this.router.navigate(['/order/address/confirm']);
  }

  loadDefaultUserAddress() {
    this.addressService.address = this.authService.user.address;
  }
}
