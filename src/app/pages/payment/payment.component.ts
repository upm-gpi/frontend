import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AddressService } from 'src/app/services/address.service';
import { ModalService } from 'src/app/services/modal.service';
import { OrdersService } from 'src/app/services/orders.service';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';
import { TimeService } from 'src/app/services/time.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {
  loading: boolean = false;
  createOrderSubscription: Subscription;
  totalPrice: number = 0;

  constructor(
    private router: Router,
    public location: Location,
    private ordersService: OrdersService,
    private modalService: ModalService,
    private shoppingCartService: ShoppingCartService,
    private addressService: AddressService,
    private timeService: TimeService
  ) {}

  ngOnInit(): void {
    this.totalPrice = this.shoppingCartService.totalAmount();
  }

  navigateToOrders() {
    this.router.navigate(['/orders']);
  }

  validate() {
    var regex = /^[0-9\s]{16,19}$/;
    let cc_value = (<HTMLInputElement>document.getElementById('ccn')).value;
    if (this.totalPrice > 11 && regex.test(cc_value)) {
      this.completeOrder();
    } else {
      if (!regex.test(cc_value)) {
        this.modalService.openGenericModal({
          title: 'FORMATO INCORRECTO',
          message:
            'Introduce un formato de tarjeta correto. Formato: 0000 0000 0000 0000',
        });
      } else {
        this.modalService.openGenericModal({
          title: 'FORMATO INCORRECTO',
          message: 'El pedido debe ser igual o superior a 12 euros.',
        });
      }
    }
  }

  completeOrder() {
    this.loading = true;

    this.createOrderSubscription = this.ordersService
      .createNewOrder(
        this.timeService.estimatedDeliveryTime?.getTime(),
        this.addressService.address
      )
      .subscribe({
        next: () => this.router.navigate(['/orders']),
        error: () =>
          this.modalService.openGenericModal({
            title: 'Realizar pedido',
            message:
              'No se ha podido crear el pedido. Por favor, revise el carrito de compras',
          }),
      })
      .add(() => (this.loading = false));
  }

  ngOnDestroy() {
    this.createOrderSubscription && this.createOrderSubscription.unsubscribe();
  }
}
