import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddressService } from 'src/app/services/address.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-address-confirm',
  templateUrl: './address-confirm.component.html',
  styleUrls: ['./address-confirm.component.scss'],
})
export class AddressConfirmComponent implements OnInit {
  constructor(private router: Router, public location: Location, public addressService: AddressService, public authService: AuthService) {}

  ngOnInit(): void {}
  getAddress():string{
    return this.addressService.address
    
  }
  navigateToTime() {
    this.router.navigate(['/order/time']);
  }
}
