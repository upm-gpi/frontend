import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Product } from 'src/app/model/Product';
import { ProductService } from 'src/app/services/product.service';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';

@Component({
  selector: 'app-select-product',
  templateUrl: './select-product.component.html',
  styleUrls: ['./select-product.component.scss'],
})
export class SelectProductComponent implements OnInit {
  category: any;
  products$: Observable<Product[]>;

  constructor(
    private route: ActivatedRoute,
    private productApi: ProductService,
    public shoppingCartService: ShoppingCartService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.category = params['category'];
    });
    this.products$ = this.productApi.getProductsBy({
      category: this.category,
      inMenu: true,
    });
  }
}
