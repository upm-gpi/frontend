import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TimeService } from 'src/app/services/time.service';

@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.scss'],
})
export class TimeComponent implements OnInit {
  hour: number = this.getCurrentHour() + 1;
  minutes: number = this.getCurrentMinutes();

  constructor(
    private router: Router,
    public location: Location,
    private timeService: TimeService
  ) {}

  ngOnInit(): void {}

  navigateToPayment() {
    this.timeService.estimatedDeliveryTime = this.getTimeAsDate();
    this.router.navigate(['/order/payment']);
  }

  getCurrentHour() {
    return new Date().getHours();
  }

  getCurrentMinutes() {
    return new Date().getMinutes();
  }

  loadClosestDeliveryTime() {
    this.hour = this.getCurrentHour() + 1;
    this.minutes = this.getCurrentMinutes();
  }

  private getTimeAsDate() {
    const date = new Date();
    date.setHours(this.hour);
    date.setMinutes(this.minutes);
    return date;
  }
}
