import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';
import { Product } from 'src/app/model/Product';

type MenuOption = {
  label: string;
  products: Product[];
  selected?: Product;
};
@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.scss'],
})
export class OrderSummaryComponent implements OnInit {
  constructor(
    public location: Location,
    private router: Router,
    public shoppingCartService: ShoppingCartService
  ) {}

  ngOnInit(): void {}

  navigateToAddress() {
    this.router.navigate(['/order/address']);
  }
}
