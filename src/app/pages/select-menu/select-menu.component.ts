import { Component, OnInit } from '@angular/core';
import { Menu } from 'src/app/model/Menu';
import { Product } from 'src/app/model/Product';
import { ProductService } from 'src/app/services/product.service';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';

type MenuOption = {
  label: string;
  products: Product[];
  selected?: Product;
};

@Component({
  selector: 'app-select-menu',
  templateUrl: './select-menu.component.html',
  styleUrls: ['./select-menu.component.scss'],
})
export class SelectMenuComponent implements OnInit {
  options: MenuOption[] = [];
  currentMenu: Menu;

  constructor(
    private productsService: ProductService,
    public shoppingCartService: ShoppingCartService
  ) {}

  ngOnInit(): void {
    this.productsService
      .getDailyMenu()
      .subscribe(({ starters, mains, desserts, drinks }) => {
        this.options.push({
          label: 'Entrantes',
          products: starters,
        });
        this.options.push({
          label: 'Principales',
          products: mains,
        });
        this.options.push({
          label: 'Postres',
          products: desserts,
        });
        this.options.push({ label: 'Bebidas', products: drinks });
      });
  }

  isMenuSelected(): boolean {
    return this.options.every((option) => option.selected);
  }

  handleSelection() {
    if (this.isMenuSelected()) {
      const [starter, main, dessert, drink] = this.options.map(
        (option) => option.selected
      );

      this.currentMenu = new Menu({
        starter,
        main,
        dessert,
        drink,
      });
    }
  }
}
