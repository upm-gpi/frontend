import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

/* Servicios */
import { ApiService } from 'src/app/services/api.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-select-category',
  templateUrl: './select-category.component.html',
  styleUrls: ['./select-category.component.scss'],
  providers: [ApiService],
})
export class SelectCategoryComponent implements OnInit {
  categories$: Observable<string[]>;

  constructor(private productService: ProductService) {}

  ngOnInit(): void {
    this.categories$ = this.productService.getCategories();
  }
}
