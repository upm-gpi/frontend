import { Product } from './Product';
import { User } from './User';

export enum OrderState {
  recibido = 'recibido',
  preparando = 'preparando',
  entregando = 'entregando',
  entregado = 'entregado',
}

export class Order {
  id: number;
  date: number;
  deliveryTime: number;
  shippingAddress: string;
  amount: number;
  state: OrderState;
  user?: User;
  products: { product: Product; amount: number }[];
  menus: { menu: { id: number; products: Product[] }; amount: number }[];
}
