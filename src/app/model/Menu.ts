import { Product } from './Product';

type MenuContent = {
  starter: Product;
  main: Product;
  dessert: Product;
  drink: Product;
};

export class Menu {
  static readonly IDS = new Map<MenuContent, number>();

  id: number;
  content: MenuContent;

  constructor(content: MenuContent) {
    this.content = content;

    this.id = this.getID(content);
    if (this.id === -1) {
      this.id = this.genID(content);
    }
  }

  private genID(content: MenuContent): number {
    const id =
      Menu.IDS.size > 0 ? Math.max(...Array.from(Menu.IDS.values())) + 1 : 1;

    Menu.IDS.set(content, id);

    return id;
  }

  private getID(content: MenuContent): number {
    const matchingEntry = Array.from(Menu.IDS.entries()).find(([menuContent]) =>
      this.contentEquals(content, menuContent)
    );

    if (matchingEntry) {
      const [, id] = matchingEntry;
      return id;
    } else {
      return -1;
    }
  }

  private contentEquals(c1: MenuContent, c2: MenuContent) {
    const productsIDs1 = this.getProductsIDs(c1),
      productsIDs2 = this.getProductsIDs(c2);

    return productsIDs1.every(
      (productId1, index) => productId1 === productsIDs2[index]
    );
  }

  private getProductsIDs(content: MenuContent): number[] {
    return Object.values(content).map((product) => product.id);
  }
}
