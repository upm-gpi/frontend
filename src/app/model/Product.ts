export class Product {
  id: number;
  name: string;
  category: string;
  isInMenu: boolean;
  isInDailyMenu: boolean;
  description: string;
  price: number;
}
