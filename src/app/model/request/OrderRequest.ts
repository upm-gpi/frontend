export class OrderRequest {
  deliveryTime: number; // epoch unix
  shippingAddress: string;
  amount: number;
  userId?: number; // if null then the user is not registered when creating the order
  products: { id: number; amount: number }[];
  menus: {
    amount: number;
    starterId: number;
    mainId: number;
    dessertId: number;
    drinkId: number;
  }[];
}
