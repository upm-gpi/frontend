export class RegisterRequest {
  name: string;
  email: string;
  password: string;
  address: string;
}
