import { Product } from '../Product';

export class DailyMenuResponse {
  starters: Product[];
  mains: Product[];
  desserts: Product[];
  drinks: Product[];
}
